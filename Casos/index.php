<?php
session_start();
if ($_SESSION) { ?>
  <!DOCTYPE html>
  <html lang="es">
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Bitacora de celula Lambda</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
  <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

    </head>
    <body>
      <section id="navbar">
        <div class="row-fluid">
          <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
            <div class="col-lg-6">
              <a class="navbar-brand" href="index.php">Bitacora</a>
            </div>
            <div class="col-lg-6 text-white text-capital">
              <?php

              include '../inc/operaciones.php';
              imprimirNombres();
                ?>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
                <?php echo $_SESSION['cargo'];?></a>
            </div>
        </div>
        </nav>
      </section>

  <section id="jumbotron">

    <div class="jumbotron bg-dark">
      <h2 class="text-center text-white">Estas en el modulo de control de Bitacora de Casos
        <?php imprimirNombres();?>
      </h2>
      <p class="text-center text-white">A continuacion escoja que desea realizar</p>
    </div>
  </section>

  <section id="acciones">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <h3 class="card-header bg-dark text-white">Escalados y directorio</h3>
            <div class="card-body">
              <h4 class="card-title">Crear caso / Crear bitacora de caso</h4>
              <p class="card-text">En este apartado podras crear casos que podran tramitar en el sistema</p>
              <a href="../Casos/crea_caso.php" class="btn btn-outline-dark my-2 my-sm-0">Ingresar</a>

            </div>
            <div class="card-body">
              <h4 class="card-title">Consultar caso / Consultar bitacora de casos</h4>
              <p class="card-text">En este apartado podra consultar los casos y su bitacora ademas de realizar otras funciones de
                gestion de información </p>
              <a href="../Casos/consulta_caso.php" class="btn btn-outline-dark my-2 my-sm-0">Ingresar</a>
            </div>
          </div>
        </div>

      </div>
    </div>


  </section>
  <div class="col-lg-12">&nbsp;</div>
  <div class="col-lg-12">&nbsp;</div>
  <div class="col-lg-12">&nbsp;</div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>

</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
