<?php
include '../inc/operaciones.php';
include '../inc/conexion.php';
session_start();
if($_SESSION){
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitacora de celula Lambda</title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
  <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

</head>

<body>
  <section id="navbar">
    <div class="row-fluid">
      <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
        <div class="col-lg-6">
          <a class="navbar-brand" href="index.php">Bitacora</a>
        </div>
        <div class="col-lg-6 text-white text-capital">
          <?php
              imprimirNombres();
                ?>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
            <?php echo $_SESSION['cargo'];?></a>
        </div>
    </div>
    </nav>
  </section>
  <section id="jumbotron">
    <div class="jumbotron text-center bg-dark">
      <h1 class="text-white"> Crear caso / Crear bitacora de caso</h1>
      <p class="text-white">Sr.(a)
        <?php  imprimirNombres()?> en este espacio podra crear los Casos y inicio de bitacora que se ingresara al sistema.</p>
    </div>
    <form class="form-group" action="creacion_caso.php" method="post">


  </section>
  <section id="formulario">
    <div class="container-flow">


      <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-2"><strong>Fecha inicio</strong></div>
        <div class="col-lg-3">
          <input type="date" class="form-control" name="fecha-inicio" required/>
        </div>
        <div class="col-lg-2"><strong>Usuario que reporta</strong></div>
        <div class="col-lg-3">
          <?php
          $link=conectar();
          $sql="select * from usuarios" ;
          $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
          ?>
          <select class="custom-select" name="usuario_reporta" required/>
          <option value="#">No seleccionado</option>
          <?php if($result->num_rows>0){?>
            <?php while ($r=$result->fetch_array()){
            echo "<option value=".$r["idusuarios"].">".$r['nombres']." ".$r['apellidos']."</option>";}} ?>
          </select>
        </div>
        <div class="col-lg-1">&nbsp;</div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-2"><strong>Aplicativo</strong></div>
        <div class="col-lg-3">
          <?php
          $link=conectar();
          $sql="select * from aplicativo";
          $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
          ?>
          <select class="custom-select" name="aplicativo" required/>
          <option value="#">No seleccionado</option>
          <?php if($result->num_rows>0){?>
            <?php while ($r=$result->fetch_array()){
            echo "<option value=".$r["idaplicativo"].">".$r['nombre']."</option>";}} ?>
          </select>
        </div>
        <div class="col-lg-2"><strong>Origen</strong></div>
        <div class="col-lg-3">
          <?php
          $link=conectar();
          $sql="select * from origen";
          $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
          ?>
          <select class="custom-select" name="origen" required/>
          <option value="#">No seleccionado</option>
          <?php if($result->num_rows>0){?>
            <?php while ($r=$result->fetch_array()){
            echo "<option value=".$r["idorigen"].">".$r['origen']."</option>";}} ?>
          </select>
        </div>
        <div class="col-lg-1">&nbsp;</div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-2"><strong>Tipo de atencion</strong></div>
        <div class="col-lg-3">
          <?php
          $link=conectar();
          $sql="select * from tipificacion";
          $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
          ?>
          <select class="custom-select" name="tipificacion" required/>
          <option value="#">No seleccionado</option>
          <?php if($result->num_rows>0){?>
            <?php while ($r=$result->fetch_array()){
            echo "<option value=".$r["idtipificacion"].">".$r['tipo']."</option>";}} ?>
          </select>
        </div>
        <div class="col-lg-2"><strong>Segmento afectado</strong></div>
        <div class="col-lg-3">
          <?php
          $link=conectar();
          $sql="select * from tp_producto";
          $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
          ?>
          <select class="custom-select" name="tp_producto" required/>
          <option value="#">No seleccionado</option>
          <?php if($result->num_rows>0){?>
            <?php while ($r=$result->fetch_array()){
            echo "<option value=".$r["idtp_producto"].">".$r['nombre']."</option>";}} ?>
          </select>
        </div>
        <div class="col-lg-1">&nbsp;</div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-2"><strong>Descripción</strong></div>
        <div class="col-lg-8">
          <textarea name="descripcion" class="form-control" rows="8" cols="10" required></textarea>
        </div>
        <div class="col-lg-1">&nbsp;</div>
      </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">

    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3">
      <input type="submit" class="btn btn-dark" name="" value="Crear Caso">
    </div>
    <div class="col-lg-3">&nbsp;</div>
  </div>
  </section>
  </form>
  <section>
    <div class="row">&nbsp;</div>

  </section>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
