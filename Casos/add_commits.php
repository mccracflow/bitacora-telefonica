<?php
include '../inc/operaciones.php';
include '../inc/conexion.php';
session_start();
if($_SESSION){
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitacora de celula Lambda</title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
  <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

</head>

<body>
  <section id="navbar">
    <div class="row-fluid">
      <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
        <div class="col-lg-6">
          <a class="navbar-brand" href="index.php">Bitacora</a>
        </div>
        <div class="col-lg-6 text-white text-capital">
          <?php
              imprimirNombres();
                ?>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
            <?php echo $_SESSION['cargo'];?></a>
        </div>
    </div>
    </nav>
  </section>
  <section id="jumbotron">
    <div class="jumbotron text-center bg-dark">
      <h1 class="text-white"> Añadir comentarios a el caso <?php echo $_GET['idcasos']; ?> </h1>
      <p class="text-white">Sr.(a)
        <?php  imprimirNombres()?> en este espacio podra crear y visualizar los comentarios realizados para el caso <?php echo $_GET['idcasos']; ?>.</p>
    </div>
    <form class="form-group" action="add_commit.php" method="post">

  <section>
<div class="row">
  <div class="col-lg-1">&nbsp;</div>
  <div class="col-lg-2">
  <strong>Añadir comentario de caso</strong><hr>
  <input type="submit" name="comentario" value="Agregar comentario" class="btn btn-outline-dark">
  </div>
  <div class="col-lg-8"><textarea name="comentario" rows="4" cols="80" class="form-control"></textarea></div>
  <div class="col-lg-1">&nbsp;</div>
</div>
</form>
<div class="row">
<div class="col-lg-1">&nbsp;</div>
<div class="col-lg-10">
  <table class="table table-bordered">
  <tbody>
    <tr>
<?php
$_SESSION['idcasos']=$_GET['idcasos'];
$link=conectar();

$sql='SELECT * FROM `comentarios`, `usuarios` WHERE comentarios.casos_idcasos = "'.$_GET['idcasos'].'" AND usuarios.idusuarios = comentarios.usuarios_idusuarios ORDER BY Fecha DESC';
$result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));

?>
<?php if($result->num_rows>0){?>
<table class="table table-bordered table-hover">
<?php  while($r=$result->fetch_array()){?>

<tr>
	<td>
    <div class="alert alert-dark" role="alert">
    Comentario realizado por <?php echo $r['nombres'] ." ".$r['apellidos'] ; ?> en la fecha  <?php echo $r['Fecha']; ?>
  </div>
    <?php echo $r["comentario"]; ?></td>


    </div>
  </div>

    <?php }
  } else {
    echo "<hr>NO SE ENCONTRARON RESULTADOS";
    ?>
    <?php } ?>
</div>
<div class="col-lg-1">&nbsp;</div>
</div>


  </section>
  <div class="row">&nbsp;</div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
