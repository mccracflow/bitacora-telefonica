<?php
include '../inc/operaciones.php';
include '../inc/conexion.php';
session_start();
if($_SESSION){
  ?>
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Bitacora de celula Lambda</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
  <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<script language="javascript">
$(document).ready(function(){
    $("#estado").on('change', function () {
        $("#estado option:selected").each(function () {
            elegido=$(this).val();
            $.post("subestado.php", { elegido: elegido }, function(data) {
                $("#subestado").html(data);
            });
        });
   });
});
</script>
</head>

<body>
  <section id="navbar">
    <div class="row-fluid">
      <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
        <div class="col-lg-6">
          <a class="navbar-brand" href="index.php">Bitacora</a>
        </div>
        <div class="col-lg-6 text-white text-capital">
          <?php
          imprimirNombres();
          $_SESSION['idcasos']=$_GET['idcasos'];
            ?>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
            <?php echo $_SESSION['cargo'];?></a>
        </div>
    </div>
    </nav>
  </section>
 <section id="jumbotron">
<?php
   function imprimirnombre(){
   echo "<script type='text/javascript'>
           document.write('".$_SESSION['nombre']." ".$_SESSION['apellido']. ".');
         </script>";
   }

?>
     <div class="jumbotron bg-dark text-center text-white">
       <h2>Bienvenido <?php imprimirnombre();?></h2>
       <p>A continuacion podras realizar las operaciones correspondientes a los estados</p>
     </div>

 </section>

<section id=insertar>
<div class="container">
 <div class="row">
   <div class="col-lg-10 text-center">
     <h5>Para agregar algun esatdo ir a el apartado de crear estados o seguir el siguiente boton</h5>
   </div>
   <div class="col-lg-2">
     <a href="crea_estados.php" class="btn btn-dark ">Crear estados</a>
     </div>
   </div>
 </div>
</div>
</section>

<div class="container">
  <div class="row">&nbsp;</div>
  <div class="row">&nbsp;</div>
  <div class="row">&nbsp;</div>


    <div class="row">
      <div class="col-lg-2">&nbsp;</div>
        <div id="content" class="col-lg-8">
            <form action="update_state.php" method="post">
              <div class="row">
                <div class="form-group col-lg-6">
                    <label for="estado">Estado</label>
                    <?php
                    $link=conectar();
                    $sql='select * from estado';
                    $result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));
                    ?>
                    <select class="custom-select" name="estado" id="estado" >
                    <?php if($result->num_rows>0){?>
                      <?php while ($r=$result->fetch_array()){
                      echo "<option value=".$r["idestado"].">".$r['estado']."</option>";}} ?>
                    </select>
                </div>
                <div class="form-group col-lg-6">
                    <label for="curso">Subestado</label>
                    <select name="subestado" id="subestado" class="form-control">

                    </select>
                </div>
              </div>
              <div class="row">&nbsp;</div>
              <div class="row">
                <div class="col-lg-5">&nbsp;</div>
                <div class="col-lg-2">
                  <input type="submit" name="actualizar" value="Actualizar estado del caso" class="btn btn-outline-dark">
                </div>
                <div class="col-lg-5">&nbsp;</div>
              </div>
            </form>


        </div>
        <div class="col-lg-3">&nbsp;</div>
    </div>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
