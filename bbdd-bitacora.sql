-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-10-2018 a las 17:19:56
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bitacora`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aplicativo`
--

CREATE TABLE `aplicativo` (
  `idaplicativo` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos`
--

CREATE TABLE `casos` (
  `idcasos` int(11) NOT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `Fecha_inicio` varchar(45) DEFAULT NULL,
  `Fecha_Fin` varchar(45) DEFAULT NULL,
  `Solucion` varchar(45) DEFAULT NULL,
  `usuarios_reporto` int(11) NOT NULL,
  `usuarios_responsable` int(11) NOT NULL,
  `tipificacion_idtipificacion` int(11) NOT NULL,
  `escalado_idescalado` int(11) NOT NULL,
  `origen_idorigen` int(11) NOT NULL,
  `estado_idestado` int(11) NOT NULL,
  `tp_producto_idtp_producto` int(11) NOT NULL,
  `aplicativo_idaplicativo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escalado`
--

CREATE TABLE `escalado` (
  `idescalado` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idestado` int(11) NOT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `usuarios_idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`correo`, `password`, `usuarios_idusuarios`) VALUES
('fabian@gmail.com', '987654', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen`
--

CREATE TABLE `origen` (
  `idorigen` int(11) NOT NULL,
  `origen` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipificacion`
--

CREATE TABLE `tipificacion` (
  `idtipificacion` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tp_producto`
--

CREATE TABLE `tp_producto` (
  `idtp_producto` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `Documento` varchar(45) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) NOT NULL,
  `area` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombres`, `apellidos`, `Documento`, `correo`, `telefono`, `area`) VALUES
(1, 'Hector Fabian', 'Rodriguez Acosta', '2353647589', 'fabian@gmail.com', '2345323', 'Desarrollo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aplicativo`
--
ALTER TABLE `aplicativo`
  ADD PRIMARY KEY (`idaplicativo`);

--
-- Indices de la tabla `casos`
--
ALTER TABLE `casos`
  ADD PRIMARY KEY (`idcasos`),
  ADD KEY `fk_casos_usuarios1_idx` (`usuarios_reporto`),
  ADD KEY `fk_casos_tipificacion1_idx` (`tipificacion_idtipificacion`),
  ADD KEY `fk_casos_escalado1_idx` (`escalado_idescalado`),
  ADD KEY `fk_casos_usuarios2_idx` (`usuarios_responsable`),
  ADD KEY `fk_casos_origen1_idx` (`origen_idorigen`),
  ADD KEY `fk_casos_estado1_idx` (`estado_idestado`),
  ADD KEY `fk_casos_tp_producto1_idx` (`tp_producto_idtp_producto`),
  ADD KEY `fk_casos_aplicativo1_idx` (`aplicativo_idaplicativo`);

--
-- Indices de la tabla `escalado`
--
ALTER TABLE `escalado`
  ADD PRIMARY KEY (`idescalado`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`correo`),
  ADD KEY `fk_login_usuarios_idx` (`usuarios_idusuarios`);

--
-- Indices de la tabla `origen`
--
ALTER TABLE `origen`
  ADD PRIMARY KEY (`idorigen`);

--
-- Indices de la tabla `tipificacion`
--
ALTER TABLE `tipificacion`
  ADD PRIMARY KEY (`idtipificacion`);

--
-- Indices de la tabla `tp_producto`
--
ALTER TABLE `tp_producto`
  ADD PRIMARY KEY (`idtp_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aplicativo`
--
ALTER TABLE `aplicativo`
  MODIFY `idaplicativo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `escalado`
--
ALTER TABLE `escalado`
  MODIFY `idescalado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `idestado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `origen`
--
ALTER TABLE `origen`
  MODIFY `idorigen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipificacion`
--
ALTER TABLE `tipificacion`
  MODIFY `idtipificacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tp_producto`
--
ALTER TABLE `tp_producto`
  MODIFY `idtp_producto` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `casos`
--
ALTER TABLE `casos`
  ADD CONSTRAINT `fk_casos_aplicativo1` FOREIGN KEY (`aplicativo_idaplicativo`) REFERENCES `aplicativo` (`idaplicativo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_escalado1` FOREIGN KEY (`escalado_idescalado`) REFERENCES `escalado` (`idescalado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_estado1` FOREIGN KEY (`estado_idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_origen1` FOREIGN KEY (`origen_idorigen`) REFERENCES `origen` (`idorigen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_tipificacion1` FOREIGN KEY (`tipificacion_idtipificacion`) REFERENCES `tipificacion` (`idtipificacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_tp_producto1` FOREIGN KEY (`tp_producto_idtp_producto`) REFERENCES `tp_producto` (`idtp_producto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_usuarios1` FOREIGN KEY (`usuarios_reporto`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_casos_usuarios2` FOREIGN KEY (`usuarios_responsable`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `fk_login_usuarios` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
