<?php
include '../inc/operaciones.php';
include '../inc/conexion.php';

session_start();
if($_SESSION){
  ?>


  <?php
$link=conectar();
$_SESSION['idusuario']=$_GET['idusuario'];
$sql='SELECT * FROM usuarios WHERE Documento="'.$_GET['idusuario'].'"';
$result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));

  if($row=mysqli_fetch_array($result)){

$nombre=$row['nombres'];
$Apellido=$row['apellidos'];
$Cargo=$row['area'];
$Telefono=$row['telefono'];
$Correo=$row['correo'];
}

   ?>
   <!DOCTYPE html>
   <html lang="en">
     <head>
       <!-- Required meta tags -->
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <title>Bitacora de celula Lambda</title>
       <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
   <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

     </head>
     <body>
  <section id="navbar">
    <div class="row-fluid">
      <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
        <div class="col-lg-6">
          <a class="navbar-brand" href="index.php">Bitacora</a>
        </div>
        <div class="col-lg-6 text-white text-capital">
          <?php

          imprimirNombres();
            ?>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <span>&nbsp;</span>
          <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
            <?php echo $_SESSION['cargo'];?></a>
        </div>
    </div>
    </nav>
  </section>
 <section id="jumbotron">
  <div class="jumbotron text-center text-white bg-dark">
    <h1> Actualizar Usuario</h1>
    <p>Sr.(a) <?php  imprimirNombres()?> podra consultar usuarios que podran ingresar al sistema.</p>
  </div>
<form class="form-group" action="actualiza_usuarios.php" method="post">


 </section>
<section id="formulario">
<div class="container">
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Nombre</strong></div>

    <div class="col-lg-4">
      <input type="text" name="nombre_usuario" class="form-control" pattern="[A-Za-z ]+" placeholder="Nombre" value="<?php echo $nombre ;?>">
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Apellido</strong></div>

    <div class="col-lg-4">
      <input type="text" name="apellido_usuario" class="form-control" pattern="[A-Za-z ]+" placeholder="Apellido" value="<?php echo $Apellido; ?>">
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>

  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Cargo</strong></div>

    <div class="col-lg-4">
      <input type="text" name="cargo_usuario" class="form-control" placeholder="Cargo" value="<?php  echo $Cargo ?>">
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Telefono</strong></div>

    <div class="col-lg-4">
      <input type="text" name="telefono_usuario" class="form-control" pattern="[0-9]+" placeholder="Telefono" value="<?php echo $Telefono ?>">
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Correo</strong></div>

    <div class="col-lg-4">
      <input type="text" name="correo_usuario" class="form-control" placeholder="Correo" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="<?php echo $Correo?>" >
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>

  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-4">&nbsp;</div>
    <div class="col-lg-4">&nbsp;</div>
    <div class="col-lg-4">
      <input type="submit" class="btn btn-outline-dark" value="Actualizar usuario">
    </div>
  </div>
</div>
</section>

</form>
<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>
<div class="row">&nbsp;</div>


</body>
</html>
<?php

}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
