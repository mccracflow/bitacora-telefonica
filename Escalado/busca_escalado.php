<?php
session_start();
if ($_SESSION) {
 ?>
 <!DOCTYPE html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>Bitacora de celula Lambda</title>
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
 <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

   </head>
   <body>
     <section id="navbar">
       <div class="row-fluid">
         <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
           <div class="col-lg-6">
             <a class="navbar-brand" href="index.php">Bitacora</a>
           </div>
           <div class="col-lg-6 text-white text-capital">
             <?php
             include '../inc/operaciones.php';
             imprimirNombres();
               ?>
             <span>&nbsp;</span>
             <span>&nbsp;</span>
             <span>&nbsp;</span>
             <span>&nbsp;</span>
             <span>&nbsp;</span>
             <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
               <?php echo $_SESSION['cargo'];?></a>
           </div>
       </div>
       </nav>
     </section>
    <section id="jumbotron">
  <?php
      function imprimirnombre(){
      echo "<script type='text/javascript'>
              document.write('".$_SESSION['nombre']." ".$_SESSION['apellido']. ".');
            </script>";
      }

?>
        <div class="jumbotron bg-dark text-center text-white">
          <h2>Bienvenido <?php imprimirnombre();?></h2>
          <p>A continuacion podras realizar las operaciones correspondientes a los tipos de producto</p>
        </div>

    </section>

<section id=insertar>
  <div class="container">
    <div class="row">
      <div class="col-lg-10 text-center">
        <h5>Para agregar algun tipo de producto ir a el apartado de crear tipo de producto o seguir el siguiente boton</h5>
      </div>
      <div class="col-lg-2">
        <a href="crea_escalado.php" class="btn btn-dark ">Crear Tipo de producto</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="row">&nbsp;</div>
  <div class="container">

  <form class="navbar-form navbar-left" role="search" action="busca_escalado.php">

      <div class="row">

      <div class="col-lg-10">
      <input type="text" name="s" class="form-control" placeholder="Buscar">
      </div>
      <div class="col-lg-2">
        <input type="submit" value="Busqueda especial" class="btn btn-dark">
      </div>
    </div>
    </div>


    </div>
  </form>
</div>
</section>
<section>

  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="container">

<?php
include '../inc/conexion.php';
$link=conectar();

$sql='SELECT * FROM escalado WHERE idescalado like "%'.$_GET['s'].'%" or nombre like "%'.$_GET['s'].'%" or correo like "%'.$_GET['s'].'%" or area like "%'.$_GET['s'].'%" or telefono like "%'.$_GET['s'].'%" ';
$result=mysqli_query($link,$sql) or die ("ERROR en la Consulta $sql".mysqli_error($link));

?>
<?php if($result->num_rows>0){?>
<table class="table table-bordered table-hover ">
<thead class="thead-dark">
	<th>Id tipo de producto</th>
	<th>Nombre</th>
  <th>Correo</th>
  <th>Area</th>
  <th>Telefono</th>

  <th>Operaciones</th>
</thead>
<?php  while($r=$result->fetch_array()){?>
<tr>
	<td><?php echo $r["idescalado"]; ?></td>
	<td><?php echo $r["nombre"]; ?></td>
  <td><?php echo $r["correo"]; ?></td>
  <td><?php echo $r["area"]; ?></td>
  <td><?php echo $r["telefono"]; ?></td>


	<td>
<?php if ($_SESSION['cargo']=="Desarrollo") {
   ?>
        <a href="updat_escalado.php?idescalado=<?php echo $r["idescalado"];?>" class="btn btn-sm btn-success">Actualizar</a><br>
    		<a href="eliminar_escalado.php?idescalado=<?php echo $r["idescalado"];?>" class="btn btn-sm btn-danger">Eliminar</a><br>
<?php }else{ ?>
  <a href="updat_escalado.php?idescalado=<?php echo $r["idescalado"];?>" class="btn btn-sm btn-success">Actualizar</a><br>
  <?php } ?>
      </div>
    </div>

    <?php }
  } else {
    echo "NO SE ENCONTRARON RESULTADOS";
    ?>
    <?php } ?>
	</td>
</tr>
</table>

</div>
</div>
</section>



<section>
  <div class="row">&nbsp;</div>
  <div class="row">&nbsp;</div>
  <div class="row">&nbsp;</div>

</section>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
