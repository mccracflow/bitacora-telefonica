<?php
include '../inc/operaciones.php';
include '../inc/conexion.php';
session_start();
if($_SESSION){
  ?>
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Bitacora de celula Lambda</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
  <link rel="stylesheet" href="../css/bootstrap4-business-tycoon.min.css">

    </head>
    <body>
      <section id="navbar">
        <div class="row-fluid">
          <nav class="text-center navbar navbar-inverse navbar-toggleable-md navbar-dark bg-dark">
            <div class="col-lg-6">
              <a class="navbar-brand" href="index.php">Bitacora</a>
            </div>
            <div class="col-lg-6 text-white text-capital">
              <?php
              imprimirNombres();
                ?>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <span>&nbsp;</span>
              <a href="../inc/salir.php" class="btn btn-outline-danger my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i>Cerrar Sesion
                <?php echo $_SESSION['cargo'];?></a>
            </div>
        </div>
        </nav>
      </section>
 <section id="jumbotron">
  <div class="jumbotron text-center bg-dark">
    <h1 class="text-white"> Crear estado nuevo</h1>
    <p class="text-white">Sr.(a) <?php  imprimirNombres()?> en este espacio podra crear los estados de la información que ingresaran al sistema.</p>
  </div>
<form class="form-group" action="creacion_estados.php" method="post">


 </section>
<section id="formulario">
<div class="container">
  <div class="row">&nbsp;</div>

  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Codigo de Estado</strong></div>

    <div class="col-lg-4">
      <input type="number" name="codigo" class="form-control" pattern="[0-9]+" placeholder="Codigo" required/>
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-3">&nbsp;</div>
    <div class="col-lg-3"><strong>Estado</strong></div>

    <div class="col-lg-4">
      <input type="text" name="estado" class="form-control" pattern="[A-Za-z ]+" placeholder="Estado" required/>
    </div>
    <div class="col-lg-2">&nbsp;</div>
  </div>
  <div class="row">&nbsp;</div>
  <div class="row">
    <div class="col-lg-4">&nbsp;</div>
    <div class="col-lg-4">&nbsp;</div>
    <div class="col-lg-4">
      <input type="submit" class="btn btn-dark" value="Crear estado">
    </div>
  </div>
</div>
</section>
</form>
<section>
  <div class="row">&nbsp;</div>

</section>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
else {
	echo "<script type='text/javascript'>
		alert('Ud no ha iniciado sesion. Por favor iniciar una o registrese');
		window.location='/index.html';
	</script>";
} ?>
